import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

import Button from '../components/Button';

const fader = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const Container = styled.div`
	width: 75%;
	padding: 25px;
	margin: 0 auto;
`;

const QuoteText = styled.h1`
	text-shadow: 1px 1px 1px rgba(158, 37, 37, 1);
	animation: 1s ${fader} alternate;

	&::first-letter {
		font-size: 70px;
	}
`;

const SingleQuote = ({ data, next }) => (
  <Container>
    <QuoteText>{data.quote} - <span>{data.author}</span></QuoteText>
    <Button next={next}>nextQuote</Button>
  </Container>
);

SingleQuote.propTypes = {
  data: PropTypes.shape({
    quote: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
  }).isRequired,
  next: PropTypes.func.isRequired
};

export default SingleQuote;
