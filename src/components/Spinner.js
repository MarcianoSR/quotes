import React from 'react';
import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  to {transform: rotate(360deg);}
`;

const Loading = styled.div`
	&:before {
		content: '';
		position: absolute;
		top: 25%;
		left: 50%;
		width: 50px;
		height: 50px;
		margin-top: -10px;
		margin-left: -10px;
		border-radius: 50%;
		border-top: 2px solid white;
		border-right: 2px solid transparent;
		animation: ${spin} .6s linear infinite;
	}
`;

const Spinner = props => (
  <Loading />
);

export default Spinner;
