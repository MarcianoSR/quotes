import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PrimaryButton = styled.button`
	cursor: pointer;
	border: none;
	background: none;
	color: white;
	font-size: 18px;
	font-family: 'Montserrat';
	margin-top: 10px;
	float: right;
`;

const Button = props => (
  <PrimaryButton onClick={props.next}>{props.children}</PrimaryButton>
);

Button.propTypes = {
  next: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired
};

export default Button;
