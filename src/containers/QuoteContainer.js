import React, { Component } from 'react';

import axios from 'axios';

import SingleQuote from '../components/SingleQuote';
import Spinner from '../components/Spinner';

class QuoteContainer extends Component {
  constructor () {
    super();

    this.state = {
      quotes: null,
      loading: true
    };

    this.nextQuote = this.nextQuote.bind(this);
  }

  componentDidMount () {
    setTimeout(() => {
      this.fetchQuotesFromApi();
    }, 5000);
  }

  async fetchQuotesFromApi () {
    try {
      const quotes = await axios.get('http://quotes.stormconsultancy.co.uk/quotes.json');

      this.setState({ quotes: quotes.data, loading: false });
    }	catch (error) {
      this.setState({ quotes: null, error, loading: false });
    }
  }

  nextQuote () {
    const quotes = this.state.quotes;
    const randomNumber = Math.floor(Math.random() * quotes.length);

    quotes.splice(randomNumber, 1);

    this.setState({ quotes });
  }

  render () {
    const loading = this.state.loading;
    const quotes = this.state.quotes;
    const error = this.state.error;
    let quotesState;

    if (loading) {
      quotesState = <Spinner>Is aan het laden...</Spinner>;
    }

    if (quotes && quotes.length === 0) {
      quotesState = <div> Er zijn geen quotes gevonden. </div>;
    }

    if (quotes && quotes.length > 0) {
      const randomSingleQuote = quotes[Math.floor(Math.random() * quotes.length)];

      quotesState = (
        <section>
          <SingleQuote next={this.nextQuote} data={randomSingleQuote} />
        </section>
			);
    }	else if (error) {
      quotesState = <div> Er is iets misgegaan met het ophalen van de quotes. Probeer het later opnieuw. </div>;
    }

    return quotesState;
  }
}

export default QuoteContainer;
