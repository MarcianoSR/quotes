import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import styled from 'styled-components';

import './global';

import Home from './components/Home';

const Title = styled.h1`
	text-align: center;
	font-size: 50px;
	margin-top: 25px;
`;

const initializeApp = () => {
  render(
    <div>
      <Title>I love Quotes</Title>
      <Home />
    </div>
		,
    document.querySelector('#root'));
};

initializeApp();
