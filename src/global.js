/* Global styles */
import { injectGlobal } from 'styled-components';

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,700');

  *, *:before, *:after {
    box-sizing: border-box;
  }

  body {
    margin: 0;
		background-color: lightcoral;
    font-family: 'Montserrat', 'Calibri ';
  }

	h1 {
		margin: 0;
		color: white;
		font-size: 30px;

	}

	p {
		color: white;
		font-size: 18px;
		line-height: 24px;
	}

`;
