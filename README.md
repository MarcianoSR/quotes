# Quotes app
A simple project to demonstrate a bit of my technical skills.

## Front-end stack:
- React
- Styled components
- Webpack 3
- Babel transpiler
- Eslinter
- Husky (pre-commit lint checker)
- Yarn package manager

### Run the projects dev server:

Run `yarn run dev:server` and a server will spin up at localhost:9000.
